registerServiceWorker();

// Create new link or select app according to hostname
async function openApp() {
  if (hostname === 'localhost' || hostname.endsWith('web-app.localhost')) {

    var randomNumber = getRandom();
    var orig = window.origin;
    var url = orig.replace(hostname, randomNumber + ".localhost") + "/index.html";
    window.open(url);

  } else {
    fileInput.click();
  }
}

// // let resolveAppFilePromise;
// // const appFilePromise = new Promise(r => r = resolveAppFilePromise);
// /** @type {Promise<File>} */
// const appFilePromise = new Promise(r => {
//   window.addEventListener("message", event => {
//     if (event.data?.type !== "file") {
//       return;
//     }
//     r(new File([event.data.fileContent], event.data.fileName));
//   });
// });

// appFilePromise.then(getApp);

getApp()

async function getApp() {
  // /** @type {File} */
  // const selectedFile = fileInput.files[0];

  // const searchParams = new URLSearchParams(document.location.search);
  // const blobUrl =
  //   "data:application/octet-stream;base64," +
  //   searchParams.get("fileContentsBase64");
  // const fileName_ = searchParams.get("fileName")


  // File contents in a "hash" of a URL.

  // const pos = location.hash.indexOf("/")
  // const [fileName_, fileContentsBase64] = [
  //   location.hash.slice(1, pos),
  //   location.hash.slice(pos + 1),
  // ]
  // window.location.hash = "";
  // const blobUrl = "data:application/octet-stream;base64," + fileContentsBase64;

  // const fileContentBlob = await (await fetch(blobUrl)).blob();
  // const selectedFile = new File([fileContentBlob], fileName_);


  // Download link.
  let fileLink = location.hash.slice(1);
  try {
    new URL(fileLink);
  } catch (e) {
    fileLink = atob(location.hash.slice(1));
  }
  const blob = await (await fetch(fileLink)).blob();
  const selectedFile = new File([blob], 'stub-name.xdc');




  const fileName = selectedFile.name;

  if (fileName.endsWith('.zip') || fileName.endsWith('.xdc')) {
    const zip = JSZip();
    const content = await zip.loadAsync(selectedFile);
    const list = Object.keys(content.files);
    const numbers = list.length;
    const ifIndexHtml = ifArrayHas("index.html", list);

    if (ifIndexHtml === true) {

      const cache = await caches.open("App-Cache-DataBase");


      // Debugging
      /* uncomment to add the file eruda.js to every app
      await addToCache("eruda.js", cache);
      */
      // Debugging


      await addToCache("webxdc.js", cache);


      var number = 0;

      content.forEach(async function(path, fileObj) {
        if (fileObj.dir) {
          number += 1;

          if (number === numbers) {
            location.href = "index.html";
          }

        } else {
          var fileData;
          if (path === "index.html") {

            const htmlString = await fileObj.async("string");
            const name = await getName(content, list, fileName);
            const iconName = getIcon(content, list);

            fileData = injectStuff(htmlString, name, iconName);

          } else {

            fileData = await fileObj.async("blob");

          }

          const file = getFile(fileData, path);

          const response = getResponse(file);
          await cache.put(path, response);

          number += 1;

          if (number === numbers) {
            location.href = "index.html";
          }

        }

      });

    }


  }

}